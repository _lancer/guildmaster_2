﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HireButton : MonoBehaviour
{
    //1 day is a minute;
    public Player localPlayer;
    public void d ()
    {
        if (ArmyListButton.selected == null || ArmyListButton.selected.relatedArmy.squads.Count == 5)
            //create new free army
            return;
        Squad squad = ArmyListButton.selected.relatedArmy.gameObject.AddComponent<Squad>();
        squad.hiring = true;
        localPlayer.hiringItems.Add(new HiringQueueItem(4, squad, ArmyListButton.selected.relatedArmy));
        ArmyListButton.selected.relatedArmy.squads.Add(squad); 
        localPlayer.rebuildContainer();
        if (ArmyListButton.selected.relatedArmy.squads.Count == 5)
            HireMenu.pick();
        else
            ArmyListButton.selected.rebuildUnitscontainer(true);
    }


}

using System;
using UnityEngine;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    [RequireComponent(typeof (UnityEngine.AI.NavMeshAgent))]
    [RequireComponent(typeof (ThirdPersonCharacter))]
    public class AICharacterControl : MonoBehaviour
    {
        public UnityEngine.AI.NavMeshAgent agent { get; private set; }             // the navmesh agent required for the path finding
        public ThirdPersonCharacter character { get; private set; } // the character we are controlling
        public Transform target;                                    // target to aim for

        public enum SquadOffset
        {
            central,
            right,
            left
        }
        public enum ArmyOffset
        {
            central,
            right,
            left,
            right2,
            left2
        }
        // unit position in squad
        public SquadOffset squad_offset = SquadOffset.central;
        // squad position in army
        public ArmyOffset army_offset = ArmyOffset.central;

        private void Start()
        {
            // get the components on the object we need ( should not be null due to require component so no need to check )
            agent = GetComponentInChildren<UnityEngine.AI.NavMeshAgent>();
            character = GetComponent<ThirdPersonCharacter>();

	        agent.updateRotation = false;
	        agent.updatePosition = true;
        }

        float armyOffset = 4;
        private void Update()
        {
            Vector3 sqOf = Vector3.zero;
            if (squad_offset == SquadOffset.left)
                  sqOf = -target.right;
            if (squad_offset == SquadOffset.right)
                sqOf = target.right;

            Vector3 armOf = Vector3.zero;
            if (squad_offset == SquadOffset.central)
            {
                if (army_offset == ArmyOffset.right)
                    armOf = transform.right * armyOffset;
                if (army_offset == ArmyOffset.right2)
                    armOf = transform.right * armyOffset * 2;
                if (army_offset == ArmyOffset.left)
                    armOf = -transform.right * armyOffset;
                if (army_offset == ArmyOffset.left2)
                    armOf = -transform.right * armyOffset * 2;
            }

            if (target != null)
                agent.SetDestination(target.position + sqOf + armOf);

            if (agent.remainingDistance > agent.stoppingDistance)
                character.Move(agent.desiredVelocity, false, false);
            else
                character.Move(Vector3.zero, false, false);
        }


        public void SetTarget(Transform target)
        {
            this.target = target;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 

public class PinchZoom : MonoBehaviour
{
    public float perspectiveZoomSpeed = 0.5f;         
    Camera camera;

    private void Start()
    {
        camera = Camera.main;
    }

    void Update()
    {
        // If there are two touches on the device...
        if (Input.touchCount == 2)
        {
            // Store both touches.
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            // Find the position in the previous frame of each touch.
            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            // Find the magnitude of the vector (the distance) between the touches in each frame.
            float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

            // Find the difference in the distances between each frame.
            float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

            // Otherwise change the field of view based on the change in distance between the touches.

            camera.transform.Translate(-Vector3.forward * deltaMagnitudeDiff * perspectiveZoomSpeed);
            // bounds
            Vector3 position = camera.transform.position;
           // position.y += deltaMagnitudeDiff * perspectiveZoomSpeed;
            position.y =  Mathf.Clamp(position.y, 30f, 190f);

            camera.transform.position = position;
        }
    }
}

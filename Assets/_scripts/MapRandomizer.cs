﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine; 

public class MapRandomizer : MonoBehaviour
{
    //populated trough  Network to generate maps in parralell on clients
    List<Vector2> forbidden = new List<Vector2>();
    public int seed = 1;
    float terrainSquares = 16f;
    public Terrain terrain;
    public TerrainData fresh;
    public GameObject[] mountainPrototypes;
    public GameObject villagePrototype;
    

    private void Start()
    {
        terrain.terrainData.SetAlphamaps( 0,0, fresh.GetAlphamaps(0, 0, terrain.terrainData.alphamapWidth, terrain.terrainData.alphamapHeight));
        Random.seed = seed;
        //clear trees
        forbidden.Add(new Vector2(5, 5));
        forbidden.Add(new Vector2(4, 8));
        forbidden.Add(new Vector2(11, 5));
        forbidden.Add(new Vector2(8, 12));
        forbidden.Add(new Vector2(11, 11));
        forbidden.Add(new Vector2(12, 8));
        forbidden.Add(new Vector2(5, 11));
        forbidden.Add(new Vector2(8, 4));
        terrain.terrainData.treeInstances = new List<TreeInstance>().ToArray();
        // 3 - 13 playable
        createVillages();
        for (int i = 0; i < 20; i ++)
            placeForest(Random.Range(3, 13), Random.Range(3, 13));
        forbidden.Add(new Vector2(6, 6));
        forbidden.Add(new Vector2(5, 8));
        forbidden.Add(new Vector2(10, 6));
        forbidden.Add(new Vector2(8, 11));
        forbidden.Add(new Vector2(10, 10));
        forbidden.Add(new Vector2(12, 7));
        forbidden.Add(new Vector2(6, 10));
        forbidden.Add(new Vector2(8, 5));
        for (int i = 0; i < 10; i++)
        {
            float x = Random.Range(3, 13);
            float y = Random.Range(3, 13);
            if (checkForbidden (x,y))
            {
                --i;
                continue;
            }
            placeMountain(x, y);
        }
    }
    void createVillages()
    {
        createVillagesAround(5, 5);
        createVillagesAround(4, 8);
        createVillagesAround(11, 5);
        createVillagesAround(8, 12);
        createVillagesAround(11, 11);
        createVillagesAround(12,8);
        createVillagesAround(5, 11);
        createVillagesAround(8, 4);
    }
    void createVillagesAround (float _x, float _y)
    {
        for (int i = 0; i <3; i++)
        {
            float x = _x + Random.Range(-1, 2);
            float y = _y + Random.Range(-1, 2);
            if (checkForbidden(x, y))
            {
                --i;
                continue;
            }
            placeVillage(x  , y  );

            PaintRoad(SquareToWorldPosition(_x, _y), SquareToWorldPosition(x, y));
        }
    }
    void placeVillage(float cellx, float celly)
    {
       // Debug.Log("Creating village at " + cellx + " " + celly);
        forbidden.Add(new Vector2(cellx, celly));
        GameObject village = GameObject.Instantiate(villagePrototype);
        village.transform.RotateAround(village.transform.position, Vector3.up, Random.Range(0, 360));
        village.transform.position = SquareToWorldPosition(cellx, celly);
    }

    void placeMountain (float cellx, float celly)
    {
        GameObject mountain = GameObject.Instantiate(mountainPrototypes[Random.Range(0, mountainPrototypes.Length)]);
        mountain.transform.RotateAround(mountain.transform.position, Vector3.up, Random.Range(0, 360));
        mountain.transform.position = SquareToWorldPosition(cellx, celly);
    }
    void placeForest(float cellx, float celly)
    {
     //   List<TreeInstance> trees = new List<TreeInstance>();
        for (int i = 0; i < 50; i++)
        {
            TreeInstance treeTemp = new TreeInstance();
            //16x16  
            Vector2 rnd = (Random.insideUnitCircle) * (1f / 25f);
            
            treeTemp.position = new Vector3(cellx / terrainSquares + rnd.x, 0, celly / terrainSquares + rnd.y);
            
            treeTemp.prototypeIndex = 0;
            treeTemp.widthScale = .4f;
            treeTemp.heightScale = .4f;
            treeTemp.color = Color.white;
            treeTemp.lightmapColor = Color.white;
            terrain.AddTreeInstance(treeTemp);
            treeTemp.rotation = Random.Range(0, 360) ;


        } 
        terrain.Flush();

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    bool checkForbidden (float x, float y)
    {
        foreach (Vector2 v in forbidden)
        {
            if ((v.x == x) && (v.y == y))
                return true;
        }
        return false;
    }
      Vector3 SquareToWorldPosition (float x, float y)
    {
        float posx = x * terrain.terrainData.size.x / terrainSquares;
        // generate random z position
        float posz = y * terrain.terrainData.size.z / terrainSquares;
        // get the terrain height at the random position
        float posy = Terrain.activeTerrain.SampleHeight(new Vector3(posx, 0, posz));
        return new Vector3(posx, posy, posz);
    }

    private void PaintRoad(Vector3 from, Vector3 to)
    {
        float currx = from.x;
        float currz = from.z;
        int steps = 60;
        for (int st = 0; st < steps; st++)
        {

            int x = (int)(((currx - terrain.transform.position.x) / terrain.terrainData.size.x)
             * terrain.terrainData.heightmapWidth) + Random.Range (-1,2);
            int y = (int)(((currz - terrain.transform.position.z) / terrain.terrainData.size.z)
             * terrain.terrainData.heightmapHeight) + Random.Range(-1, 2);

            //  float[,,] element = new float[3, 3, terrain.terrainData.alphamapLayers];
            //
            int sq = 2;
            float colam = .35f;
            float[,,] element = terrain.terrainData.GetAlphamaps(x, y, sq, sq);
            for (int i = 0; i < sq; i++)
            {
                for (int j = 0; j < sq; j++)
                {
                    if (element[i, j, 0]- colam >= 0)
                        element[i, j, 0] = element[i, j, 0] - colam;
                    if (element[i, j, 0] - colam <= 0)
                        element[i, j, 0] = 0;
                    if (element[i, j, 2] + colam < 1)
                        element[i, j, 2] = element[i, j, 2] + colam;
                    if (element[i, j, 2] + colam >= 1)
                        element[i, j, 2] = 1;
                    /*
                    if ((i == 0 && j == 0) ||
                        (i == 2 && j == 0) ||
                        (i == 0 && j == 2) ||
                        (i == 2 && j == 2)
                        )

                    {
                        element[i, j, 0] = 0.4f;
                        element[i, j, 1] = 0;
                        element[i, j, 2] = 0.6f;
                    }
                    
                    else if (i ==0 ||i==2||j==0||j==2)
                    {
                        element[i, j, 0] = 0.3f;
                        element[i, j, 1] = 0;
                        element[i, j, 2] = 0.7f;
                    } else
                    {

                        element[i, j, 0] = 0.4f;
                        element[i, j, 1] = 0;
                        element[i, j, 2] = 0.4f;
                    }
                    */
                }
            }

            terrain.terrainData.SetAlphamaps(x, y, element);
            currx += (to.x - from.x) / steps;
            currz += (to.z - from.z) / steps;
        }
        //terrain.Flush();
        
    }

}

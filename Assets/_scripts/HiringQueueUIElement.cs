﻿using UnityEngine;
using UnityEngine.UI;

public class HiringQueueUIElement : MonoBehaviour
{
    public Image outline;
    public GameObject unitIcon;
    public GameObject progressBar;
    public GameObject empty;
     

    void Start()
    {
        empty_();    
    }
    // call if you want to set it clear/empty
    public void empty_()
    {
        outline.enabled = false;
        unitIcon.SetActive(false);
        progressBar.SetActive(false);
        empty.SetActive(true);
    }
    // call if you want to set unit here
    public void unit(HiringQueueItem item)
    {
        //progressBar.GetComponent<Image>().fillAmount = 0;
        item.HiringQueueUIElementProgress = progressBar.GetComponent<Image>();
        item.HiringQueueUIElement  = this;
        outline.enabled = true;
        unitIcon.SetActive(true);
        progressBar.SetActive(true);
        empty.SetActive(false);
    }
}

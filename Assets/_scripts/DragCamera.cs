﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class DragCamera : MonoBehaviour
{

    // VARIABLES
    //
     
    public float panSpeed = 4.0f;       // Speed of the camera when being panned 

    private Vector3 mouseOrigin;    // Position of cursor when mouse dragging starts
    private bool isPanning;     // Is the camera being panned? 

    public int reversed = 1;
    //
    // UPDATE
    //

    void Update()
    {
       

        // Get the right mouse button
        if (Input.GetMouseButtonDown(0))
        {
            if (EventSystem.current.IsPointerOverGameObject() || EventSystem.current.IsPointerOverGameObject(0) || EventSystem.current.IsPointerOverGameObject(1))
                return;
            // Get mouse origin
            mouseOrigin = Input.mousePosition;
            isPanning = true;
        }

         

        // Disable movements on button release 
        if (!Input.GetMouseButton(0)) isPanning = false; 

         

        // Move the camera on it's XY plane
        if (isPanning)
        {
            Vector3 pos = Camera.main.ScreenToViewportPoint(Input.mousePosition - mouseOrigin);

            Vector3 move = new Vector3(pos.x * panSpeed, 0, pos.y * panSpeed);
            float heightK = transform.position.y;
            // 30  = 1, 190 = 4;
            heightK = 1 + (heightK - 30) * 3 / 160;
            transform.Translate(move * heightK * reversed, Space.World);

            if (transform.position.x <= 95)
            {
                Vector3 fix = transform.position;
                fix.x = 95;
                transform.position = fix;
            }
            if (transform.position.z >= 705)
            {
                Vector3 fix = transform.position;
                fix.z = 705;
                transform.position = fix;
            }
            if (transform.position.z <= 95)
            {
                Vector3 fix = transform.position;
                fix.z = 95;
                transform.position = fix;
            }
            if (transform.position.x >= 705)
            {
                Vector3 fix = transform.position;
                fix.x = 705;
                transform.position = fix;
            }
        }

       
    }
}

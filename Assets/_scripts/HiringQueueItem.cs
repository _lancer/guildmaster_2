﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class HiringQueueItem
{
    public float timeToDone;
    public float timeToDoneMax;
    Squad squad;

    Army army;

    public Image HiringQueueUIElementProgress;
    public HiringQueueUIElement HiringQueueUIElement;
    public HiringQueueItem (float time, Squad squad, Army army)
    {
        timeToDone = time;
        timeToDoneMax = time;
        this.squad = squad;
        this.army = army;
        
    }
    public void update01()
    {
        if (HiringQueueUIElementProgress == null) return;
        HiringQueueUIElementProgress.fillAmount = 1 - timeToDone / timeToDoneMax;
        timeToDone -= .1f;
    }

    //squad factory
    public void OnComplete (Player player)
    {
        squad.hiring = false;
        /*
         -создать юнит у ворот замка
-пересчитать позиции в армии
-отправить юнит на позицию в армии

         */
        for (int i =0; i<3; i++)
        {
            GameObject unit = player.unitsPack.unit1.Get(
            player.capital.transform.GetChild(0).position + player.capital.transform.GetChild(0).right * (i-1));

            squad.addUnit(unit);
        }
        squad.init();
        army.updateSquadPosition(squad);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HireMenu : MonoBehaviour
{
    public static bool enabled = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnEnable()
    {
        enabled = true;
        pick();
    }
    private void OnDisable ()
    {
        enabled = false;
        if (ArmyListButton.selected != null)
            ArmyListButton.selected.rebuildUnitscontainer();
    }
    public static void pick()
    {
        if (ArmyListButton.selected != null && ArmyListButton.selected.relatedArmy.squads.Count == 5)
            ArmyListButton.selected.unselect();
        foreach (ArmyListButton button in ArmyListButton.all)
        {
            if (button.relatedArmy.squads.Count < 5)
            {
                button.select(true);
                return;
            }
        }
    }
}

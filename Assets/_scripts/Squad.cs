﻿using System.Collections;
using UnityStandardAssets.Characters.ThirdPerson;
using UnityEngine;

public class Squad : MonoBehaviour
{
    // suad is a part of army and may contain more thatn one unit
    /*
     * Боец двигается к навпоинту + оффсет в юните (левее или правее) -
     * есть центральный боец относительно которого остальные левее или правее
     * 
     */ 
    public bool hiring = false;

    public int unitsRange = 0;
    // unit positions in squad
    public GameObject central;
    public GameObject left;
    public GameObject right;

    public GameObject navPoint;
     
    public void army_offset ( AICharacterControl.ArmyOffset offset)
    {
        central.GetComponent<AICharacterControl>().army_offset = offset;
          left.GetComponent<AICharacterControl>().army_offset = offset;
        right.GetComponent<AICharacterControl>().army_offset = offset;
    }
      
    // Start is called before the first frame update
    void Start()
    { 
    }

    public void init ()
    { 

        left.GetComponent<AICharacterControl>().squad_offset = AICharacterControl.SquadOffset.left;
        left.GetComponent<AICharacterControl>().target = central.transform;
        right.GetComponent<AICharacterControl>().squad_offset = AICharacterControl.SquadOffset.right;
        right.GetComponent<AICharacterControl>().target = central.transform;
    }
    // Update is called once per frame
    void Update()
    {
        
    }

    public void addUnit (GameObject unit)
    {
        if (central == null)
            central = unit;
        else if (left == null)
            left = unit;
        else if (right == null)
            right = unit;

    }
}

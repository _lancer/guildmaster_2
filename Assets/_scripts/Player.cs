﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    // limited amount of unit types
    public UnitsPack unitsPack;

    public GameObject queue;
    public List<Army> armies;
    [HideInInspector]
    public Army selectedArmy;

    public GameObject capital;

    public List<HiringQueueItem> hiringItems = new List<HiringQueueItem>();
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("updateHiringItems", .1f, .1f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void updateHiringItems ()
    {
        if (hiringItems.Count == 0) return;
        hiringItems.ForEach(i => i.update01());

        //build all
        hiringItems.ForEach(item => { if (item.timeToDone <= 0) item.OnComplete(this); });
        //refresh army view in case if there are created units;
        ArmyListButton.selected.rebuildUnitscontainer();

        if ( hiringItems.RemoveAll(i => i.timeToDone <= 0) > 0 )
            rebuildContainer();
    }
    // hiring queue container
    public void rebuildContainer()
    {
        int i = 0;
        int count =  hiringItems.Count;
        foreach (HiringQueueUIElement info in queue.transform.GetComponentsInChildren<HiringQueueUIElement>())
        {
            if (i < count)
                info.unit( hiringItems[i]);
            else
                info.empty_();
            i++;
        }

    }
}

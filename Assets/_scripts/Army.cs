﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;

public class Army : MonoBehaviour
{

   public  List<Squad> squads;

    Squad central;
    public int firstLineSquadsCount = 0;
    public int secondLineUnitsCount = 0;
    // Start is called before the first frame update
    void Start()
    {
        central = squads[0];
        squads[0].army_offset (AICharacterControl.ArmyOffset.central);
        squads[1].army_offset (AICharacterControl.ArmyOffset.right);
        squads[1].central.GetComponent<AICharacterControl>().target = central.central.transform; 
        squads[0].init();
        squads[1].init();
        firstLineSquadsCount = 2;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    // units' gameobject should be present before this call
    public void updateSquadPosition ( Squad squad)
    {
        if (firstLineSquadsCount == 0)
            squad.army_offset(AICharacterControl.ArmyOffset.central);
        else if (firstLineSquadsCount == 1)
            squad.army_offset(AICharacterControl.ArmyOffset.right);
        else if (firstLineSquadsCount == 2)
            squad.army_offset(AICharacterControl.ArmyOffset.left);
        else if (firstLineSquadsCount == 3)
            squad.army_offset(AICharacterControl.ArmyOffset.right2);
        else if (firstLineSquadsCount == 4)
            squad.army_offset(AICharacterControl.ArmyOffset.left2);
        // 
        squad.central.GetComponent<AICharacterControl>().target = central.central.transform;
        firstLineSquadsCount += 1;
    }
}

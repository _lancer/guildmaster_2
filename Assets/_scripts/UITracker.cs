﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UITracker : MonoBehaviour
{

    public GameObject track;
     
    public Vector3 offset;
    public Vector3 offsetV2;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
 
        transform.position = Camera.main.WorldToScreenPoint(track.transform.position + offset) + offsetV2;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TouchControls : MonoBehaviour {

    private float dragSpeed = .1f;
    private Vector3 initialMouse; 

    private Vector3 selectedPoint;

    private bool ignoreMouseUp = false;
    private float distanceConsideredAsDrag = 1f;

    public Player localPlayer;
    Vector3 initialCam;
    private void Start()
    {
        initialCam = Camera.main.transform.position;
    }
 
    void OnMouseDown()
    {
       // print("in on mouse down");
        if (EventSystem.current.IsPointerOverGameObject() || EventSystem.current.IsPointerOverGameObject(0) || EventSystem.current.IsPointerOverGameObject(1)) 
            return;
        /*
        if (UserInput.instanse != null && UserInput.instanse.gabarites != null)
            UserInput.instanse.gabarites.SetActive(false); 
            */
        initialMouse = new Vector3(Input.mousePosition.x, 0, Input.mousePosition.y);
        initialCam = Camera.main.transform.position;

        ignoreMouseUp = false;
    }
    public int maxDragSpeed = 16;
    void OnMouseDrag()
    {
        if (EventSystem.current.IsPointerOverGameObject() || EventSystem.current.IsPointerOverGameObject(0) || EventSystem.current.IsPointerOverGameObject(1))
            return;
        if (Input.touchCount > 1) return;
        float heightK = Camera.main.transform.position.y;
        // 30  = 1, 190 = ;
        heightK = 1 + (heightK - 30) * maxDragSpeed / 160;
        if (!ignoreMouseUp)
        {
            if (Vector3.Distance(new Vector3(Input.mousePosition.x, 0, Input.mousePosition.y), initialMouse) * dragSpeed * heightK > distanceConsideredAsDrag)
                ignoreMouseUp = true;
        }
         
        Camera.main.transform.root.position = initialCam - (new Vector3(Input.mousePosition.x, 0, Input.mousePosition.y) - initialMouse) * dragSpeed * heightK;

        Vector3 camClamped = Camera.main.transform.root.position;
        camClamped.x = Mathf.Clamp(camClamped.x, 95, 705);
        camClamped.z = Mathf.Clamp(camClamped.z, 95, 705);
       
        Camera.main.transform.root.position = camClamped;

    }

     void OnMouseUp()
    {
       // print("in on mouse up");
        if (EventSystem.current.IsPointerOverGameObject() || EventSystem.current.IsPointerOverGameObject(0) || EventSystem.current.IsPointerOverGameObject(1))
            return;
        if (!ignoreMouseUp)
        {  
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition );
           // print("not ignored before raycast");
            if (Physics.Raycast(ray, out hit, 500, LayerMask.GetMask ("Ground") ))
            {
                //    print("raycasted");
                //   print(hit.transform);
                if (localPlayer.selectedArmy != null)
                    localPlayer.selectedArmy.transform.position = hit.point;
            // Debug.Log(objectHit.name);
            /*
             UserInput.instanse.gabarites.transform.position = hit.point + Vector3.up*.5f;
             selectedPoint = hit.point;
             UserInput.instanse.buildButton.GetComponent<RectTransform>().position = Input.mousePosition;
             UserInput.instanse.openMenu(UserInput.instanse.buildButton);
             UserInput.instanse.gabarites.SetActive(true);
             */
            }
 
        }
    }
 
}

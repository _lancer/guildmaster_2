﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmyListButton : MonoBehaviour
{
    //garbage to clean
    public static List<ArmyListButton> all = new List<ArmyListButton>();
    public static ArmyListButton selected;
    public Player localPlayer;
    public Army relatedArmy;

    public GameObject fieldBanner;
    public GameObject unitsBar;

    public GameObject unitsContainer;

    public bool active = false;

    private void Start()
    {
        all.Add(this);
    }
    // Start is called before the first frame update
    public void m()
    {
        // cannot uselect if hiring
        if (active && !HireMenu.enabled) unselect();
        else select();
    }

    public void select(bool promised = false)
    {
        all.ForEach(b=>b.unselect());
        active = true;
        if (fieldBanner != null)

            fieldBanner.SetActive(true);
        unitsBar.SetActive(true);
        localPlayer.selectedArmy = relatedArmy;
        transform.localScale = new Vector3(1, 1.5f, 1);
        rebuildUnitscontainer(promised);
        selected = this;
    }

    public void unselect ()
    {
        active = false;
        if (fieldBanner!=null)
        fieldBanner.SetActive(false);
        unitsBar.SetActive(false);
        localPlayer.selectedArmy = null;
        transform.localScale = new Vector3(1, 1f, 1);
        selected = null;
    }

    public void rebuildUnitscontainer (bool promised = false)
    {
        if (HireMenu.enabled)
            promised = true;
        int i = 0;
        int squads = relatedArmy.GetComponent<Army>().squads.Count;
        foreach (UnitInArmyInfo info in unitsContainer.transform.GetComponentsInChildren<UnitInArmyInfo>())
        {
            if (i < squads)
                info.unit(relatedArmy.GetComponent<Army>().squads[i]);
            else if (promised && i < squads + 1)
            {
                info.promise_(); 
            }
            else 
                info.empty();
            i++;

        }
         
    }
}

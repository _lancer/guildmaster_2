﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitInArmyInfo : MonoBehaviour
{
    public GameObject icon;
    public Image glow;
    public Outline outline;
    public Image promise;

    public Color activColor;
    public Color outlineActivColor;
    public Color inactivColor;
    
    public void unit (Squad squad)
    {
        icon.SetActive(true);
        glow.color = activColor;
        outline.effectColor = outlineActivColor;
        if (squad.hiring)
        {
            icon.SetActive(false);
            promise.enabled = true;
            promise.color = activColor;
        } else
            promise.enabled = false;
    }
    public void empty ()
    {
        icon.SetActive(false);
        glow.color = inactivColor;
        outline.effectColor = inactivColor;
        promise.enabled = false;
    }
    public void promise_ ()
    {
        empty();
        promise.color = Color.white;
        promise.enabled = true;
    }
}
